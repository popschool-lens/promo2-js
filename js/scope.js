// variable globale éfinie dans le scope global
var myGlobalVar = 'Toto';
console.log(myGlobalVar);

myGlobalVar = 'Titi';
console.log(myGlobalVar);

function changeGlobalVar() {
  myGlobalVar = 'Tata';
}

changeGlobalVar();
console.log(myGlobalVar);

// variable globale définie dans un sous-scope
if (true) {
  var myGlobalVar2 = 'Toto';
}

console.log(myGlobalVar2);

// variable local définie dans un sous-scope
if (true) {
  let myLocalVar = 'Toto';
}

console.log(myLocalVar);
