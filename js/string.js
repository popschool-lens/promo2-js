var hello = 'Bonjour';
var name = 'Toto';

// concaténation en js ES5
var text = hello + ' ' + name;

console.log(text);

// échappement de caractères interdits
var text2 = "Prénom \"Alias\" Nom";

// échappement de caractères interdits
document.write(text2 + "<br />\n");

// interpolation en ES6
var text3 = `${hello} ${name}`;

console.log(text3);
