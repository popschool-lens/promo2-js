"use strict";

var stock = 100;

// afficher dans la console :
// - "stock indisponible" si le stock est à 0
// - "stock en faible quantité" si le stock est plus petit que 50
// - "stock disponible" si le stock est plus grand ou égal à 50

if (stock == 0) {
  console.log("stock indisponible");
} else if (stock > 0 && stock < 50) {
  console.log("stock faible");
} else if (stock >= 50) {
  console.log("stock disponible");
} else {
  // bloc optionnel
  console.log("stock négatif");
}

var hasValidAddress = true;
var hasValidatedAgreement = true;

// afficher dans la console :
// - "commande en cours" si hasValidAddress et hasValidatedAgreement sont vrais
// - "vous devez renseigner une adresse" si hasValidAddress est faux
// - "vous devez cocher la case Condition de vente" si hasValidatedAgreement est faux
// - "vous devez renseigner une adresse et vous devez cocher la case Condition de vente" si hasValidAddress et hasValidatedAgreement sont faux

if (hasValidAddress && hasValidatedAgreement) {
  console.log("commande en cours");
} else if (!hasValidAddress && hasValidatedAgreement) {
  console.log("vous devez renseigner une adresse");
} else if (hasValidAddress && !hasValidatedAgreement) {
  console.log("vous devez cocher la case Condition de vente");
} else if (!hasValidAddress && !hasValidatedAgreement) {
  console.log("vous devez renseigner une adresse et vous devez cocher la case Condition de vente");
}

var hasCreditCard = true;
var accountBalance = 100;
var itemCost = 43;

/*
un utilisateur veut faire un achat
vérifiez qu'il possède une carte bancaire ou que son compte perso est suffisamment aprovisionné
si les deux conditions sont vérifiées, privilégiez l'utilisation de osn compte perso

les messages :
- "vous n'avez pas de moyen de paiement" s'il n'y a pas de carte et que le compte perso est à zéro
- "votre compte perso a été débité de X, le nouveau solde est Y" si le compte perso est suffisamment approvisionné (qu'il y ait une carte ou non)
- "votre compte perso perso n'est pas assez approvisionné, il vous manque X" si le compte perso n'est pas suffisamment approvisionné et qu'il n'y a pas de carte
- "vous allez être redirigé sur la page de paiement de votre banque" si le compte perso n'est pas suffisamment approvisionné et qu'il y a une carte
*/

if (!hasCreditCard && accountBalance == 0) {
  console.log("vous n'avez pas de moyen de paiement");
} else if (accountBalance >= itemCost) {
  accountBalance = accountBalance - itemCost;
  console.log("votre compte perso a été débité de " + itemCost + ", le nouveau solde est " + accountBalance);
} else if (accountBalance < itemCost && !hasCreditCard) {
  console.log("votre compte perso perso n'est pas assez approvisionné, il vous manque " + (itemCost - accountBalance));
} else if (accountBalance < itemCost && hasCreditCard) {
  console.log("vous allez être redirigé sur la page de paiement de votre banque");
}

var authenticatedWithEmail = false;
var authenticatedWithPhone = true;

if (authenticatedWithEmail || authenticatedWithPhone) {
  console.log("vous êtes authentifié");
} else {
  console.log("vous n'êtes pas authentifié");
}

// sur un site de ecommerce, un utilisateur doit fournir au moins un email ou un numéro de tél pour confirmer sa commande
// vérifier si l'utilisateur peut confirmer sa commande
// sinon dites-lui qu'il doit fournir au moins une des deux informations

var hasEmail = true;
var hasPhone = false;

if (hasEmail || hasPhone) {
  console.log("vous avez confirmé votre commande");
} else {
  console.log("vous devez fournir au moins un email ou un téléphone");
}

// mini appli checklist vacance
function testGet() {
  var gas = document.getElementById("gas").checked;
  console.log(gas);

  var inputs = document.getElementsByTagName("input");
  for (var input of inputs) {
    console.log(input.checked);
  }

  inputs = document.getElementsByClassName("checklist-item");
  for (var input of inputs) {
    console.log(input.checked);
  }

  inputs = document.querySelectorAll("div label input.checklist-item");
  for (var input of inputs) {
    console.log(input.checked);
  }
}

function check() {
  var inputs = document.querySelectorAll("div label input.checklist-item");

  var result = true;
  for (var input of inputs) {
    if (!input.checked) {
      result = false;
      break;
    }
  }

  if (result) {
    document.querySelector("#message").innerHTML = "vous pouvez y aller";
  } else {
    document.querySelector("#message").innerHTML = "attendez, il reste des choses à faire";
  }
}
