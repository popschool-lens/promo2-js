var now = new Date();

console.log(now.getTimezoneOffset());
console.log(now.getFullYear());
console.log(now.getMonth()); // 0 : janvier, 11 : décembre
console.log(now.getDate());
console.log(now.getDay()); // 0 : dimanche, 6 : samedi
console.log(now.getHours());
console.log(now.getMinutes());
console.log(now.getSeconds());

// @fixme '2000-01-01 00:00:00' renvoit '1999-12-31T23:00:00'
var millenium = new Date('2000-01-01T00:00:00Z');

console.log(millenium);
