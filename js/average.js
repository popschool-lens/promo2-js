"use strict";

var data = [
  {
    id: 243,
    name: "user 2",
    scores: [76, 64, 10, 67, 64]
  },
  {
    id: 123,
    name: "user 1",
    scores: [42, 100, 54, 65, 32]
  },
  {
    id: 768,
    name: "user 3",
    scores: [93, 2, 56, 78, 100]
  }
];

/*
intégrez bootstrap

à partir de ces données :
- afficher un tableau html contenant le nom des utilisateurs et leurs scores, triés par nom d'utilisateur
- afficher un tableau html contenant le nom des utilisateurs et leur score moyen, triés par nom d'utilisateur
- afficher un tableau html contenant les 5 meilleurs scores, le nom de l'utilisateur et la position du socre, triés par ordre décroissant, puis par nom de user

indices :
- vous pouvez parcourir le tableau de données avec une boucle for (avec index, for in, for of )
- pour lire le nom :
  - avec une boucle for avec index : data[i].name
  - avec une boucle for in : data[i].name
  - avec une boucle for of : user.name
- pour lire tableau de scores :
  - avec une boucle for avec index : data[i].scores
  - avec une boucle for in : data[i].scores
  - avec une boucle for of : user.scores

exemple de création de code html en js et injection du code dans un élément

    var html = "";
    html = html + "<table>";

    for (var i = 0; i < data.length; i++) {
      html = html + "<tr>";
      html = html + "<td>" + data[i] + "</td>";
      html = html + "</tr>";
    }

    html = html + "</table>";

    document.querySelector("#id-balise-html").innerHTML = html;

*/
